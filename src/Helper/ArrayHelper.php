<?php

namespace App\Helper;

use Faker\Factory;

class ArrayHelper
{
    public function addFakerData(array $array, string $key): array
    {
        $faker = Factory::create();

        foreach ($array as &$el) {
            $el[$key] = $faker->uuid;
        }

        return $array;
    }
}