<?php

namespace App\Entity;

use App\Repository\PeopleRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: PeopleRepository::class)]
#[Groups(["serialization"])]
class People
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[SerializedName('id')]
    #[MaxDepth(1)]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    #[SerializedName('name')]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[SerializedName('gender')]
    private ?string $gender = null;

    #[ORM\Column]
    #[SerializedName('height')]
    private ?int $height = null;

    #[ORM\Column]
    #[SerializedName('mass')]
    private ?int $mass = null;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): static
    {
        $this->height = $height;

        return $this;
    }

    public function getMass(): ?int
    {
        return $this->mass;
    }

    public function setMass(int $mass): static
    {
        $this->mass = $mass;

        return $this;
    }
}
