<?php

namespace App\Service;

class DataProcessor
{
    public function removeFields(array &$data, array $fieldsToRemove): void
    {
        foreach ($data as &$item) {
            foreach ($fieldsToRemove as $field) {
                unset($item[$field]);
            }
        }
    }
}