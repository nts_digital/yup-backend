<?php

namespace App\Service;

use GuzzleHttp\Client;

class SwapiApiClient
{
    private Client $client;
    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->client = new Client();
        $this->baseUrl = $baseUrl;
    }

    public function fetchData(string $endpoint, array $queryParams = []): array
    {
        $url = $this->baseUrl . $endpoint;

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        $response = $this->client->get($url);
        return json_decode($response->getBody(), true);
    }
}