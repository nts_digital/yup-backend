<?php

namespace App\Controller;

use App\Entity\People;
use App\Helper\ArrayHelper;
use App\Repository\PeopleRepository;
use App\Service\DataProcessor;
use App\Service\SwapiApiClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/api', name: 'api_')]
class PeopleController extends AbstractController
{

    private DataProcessor $dataProcessor;
    private SwapiApiClient $swapiApiClient;
    private PeopleRepository $peopleRepository;
    private EntityManagerInterface $entityManager;
    private RequestStack $requestStack;
    private SerializerInterface $serializer;
    private ArrayHelper $arrayHelper;

    public function __construct(
        SwapiApiClient $swapiApiClient,
        DataProcessor $dataProcessor,
        PeopleRepository $peopleRepository,
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
        ArrayHelper $arrayHelper
    )
    {
        $this->swapiApiClient = $swapiApiClient;
        $this->dataProcessor = $dataProcessor;
        $this->peopleRepository = $peopleRepository;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->arrayHelper = $arrayHelper;
    }

    #[Route(path: '/people', name: 'people', methods: 'get')]
    public function fetchPeoples(): JsonResponse
    {
        $data = $this->swapiApiClient->fetchData('people/');
        $this->dataProcessor->removeFields($data['results'], ['hair_color', 'skin_color', 'eye_color', 'birth_year', 'homeworld', 'films', 'species', 'vehicles', 'starships', 'url']);


        $data = $this->arrayHelper->addFakerData($data['results'], 'id');

        return $this->json($data, 200, ["Content-Type" => "application/json"]);
    }

    #[Route(path: '/saved_people', name: 'saved_people', methods: 'get')]
    public function savedPeoples(): JsonResponse
    {
        $peoples = $this->peopleRepository->findAllData();

        $jsonData = $this->serializer->serialize($peoples, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
            'normalizers' => [new ObjectNormalizer(), new ArrayDenormalizer()],
        ]);

        return new JsonResponse($jsonData, 200, [], true);

    }

    #[Route(path: '/people', name: 'people_save', methods: 'post')]
    public function savePeople(): JsonResponse
    {
        $body = json_decode($this->requestStack->getMainRequest()->getContent());

        try {
            $people = new People();
            $people
                ->setId($body->id)
                ->setName($body->name)
                ->setHeight(intval($body->height))
                ->setMass(intval($body->mass))
                ->setGender($body->gender)
                ;

            $this->entityManager->persist($people);
            $this->entityManager->flush();

            return $this->json($people, 200, ["Content-Type" => "application/json"]);

        } catch (\Exception $exception) {
            return $this->json($exception->getMessage(), 500);
        }
    }

    #[Route(path: '/people/{id}', name: 'people_delete', methods: 'delete')]
    public function deletePeople($id): JsonResponse
    {
        try {
            $people = $this->peopleRepository->findOneBy(['id' => $id]);

            if ($people) {
                $this->entityManager->remove($people);
                $this->entityManager->flush();

                return $this->json('Success delete', 202);
            } else {
                return $this->json('Error delete', 400);
            }
        } catch (\Exception $exception) {
            return $this->json($exception->getMessage(), 500);
        }
    }

}