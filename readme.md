# The Star Wars
[![Generic badge](https://img.shields.io/badge/Версия-1.0.0-<COLOR>.svg)](https://gitlab.com/nts_digital/yup-backend.git)
[![PHP](https://img.shields.io/badge/--7A86B8?logo=php&logoColor=ffffff)](https://www.php.net/)
[![Symfony](https://img.shields.io/badge/--000000?logo=symfony&logoColor=ffffff)](https://symfony.com/)

### Requirements
- PHP >=8.1
- PostgeSQL >=16

### Запуск проекта в DEV среде

1. Переименовать файл .env.sample в .env и заменить в нем DATABASE_URL, предварительно создав базу данных в PostgeSQL
2. Выполнить установку зависимостей командой `composer install`
3. Выполнить в корне проекта в терминале команду `php bin/console make:migration` для создания файла миграции в БД
4. Выполнить в корне проекта в терминале команду `php bin/console doctrine:migrations:migrate` для выполения миграции в БД
5. Выполнить в корне проекта в терминале команду `symfony serve -d` для запуска локального DEV сервера
6. Сервер доступен по адресу [http://localhost:8000](http://localhost:8000)


### Запуск проекта в PROD среде на примере NGINX + PHP-FPM

1. Переименовать файл .env.sample в .env и заменить в нем DATABASE_URL, предварительно создав базу данных в PostgeSQL
2. Выполнить установку зависимостей командой `composer install`
3. Выполнить в корне проекта в терминале команду `php bin/console make:migration` для создания файла миграции в БД
4. Выполнить в корне проекта в терминале команду `php bin/console doctrine:migrations:migrate` для выполения миграции в БД
5. В виртуальном хосте nginx указать корневую директорию <project-root>/public и настроить работу с PHP-FPM

```                                                        
server {
        listen 80;
        root <project-root>/public;
        index index.php index.htm index.nginx-debian.html;

        location / {
                try_files $uri /index.php$is_args$args;
            }

        location ~ ^/index\.php(/|$) {
                fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
                fastcgi_param DOCUMENT_ROOT $realpath_root;
                internal;
        }
}
```
